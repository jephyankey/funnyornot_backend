from flask_restful import Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService
import logging as log


class FunFactor(Resource):

    def get(self, user_id):
        obj_user = UserService(DatabaseService())
        dict_data = obj_user.get_fun_factor(user_id)

        if "fail" in dict_data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"data": dict_data}
