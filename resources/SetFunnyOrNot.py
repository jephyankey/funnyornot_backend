from flask_restful import reqparse, Resource
from common.UserService  import UserService
from common.DatabaseService import DatabaseService
from common.PostService import PostService
import logging as log

__author__ = "henry"


class SetFunnyOrNot(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def post(self):

        self._parser.add_argument('user_id', type=int, required=True,
                                  help="You need to provide your user_id")
        self._parser.add_argument('post_id', type=int, required=True,
                                  help="You need to provide the post_id")
        self._parser.add_argument('stars', type=int, required=True,
                                  help="You need to provide the number of stars")

        args = self._parser.parse_args()
        obj_user = UserService(DatabaseService())
        bound_values = {"user_id": args['user_id'], "post_id": args['post_id'], "stars": args['stars']}
        data = obj_user.set_as_funny(bound_values)

        if len(data) == 0:
            return {"message": "Post was not set as funny or not"}, 500
        else:
            return {"data": data}