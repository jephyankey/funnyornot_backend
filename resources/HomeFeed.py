from flask_restful import reqparse, Resource
from common.FeedService import FeedService
from common.DatabaseService import DatabaseService
import logging as log


class Feed(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def get(self, user_id, contact_ids, page=1, limit=10):

        contacts = contact_ids.split(',')
        obj_feed = FeedService(DatabaseService(), user_id, page, limit)
        dict_data = obj_feed.fetch_feed(contacts)
        if "fail" in dict_data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {'data': dict_data}

