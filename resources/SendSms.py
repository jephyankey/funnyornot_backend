__author__ = "MArK"


from flask_restful import Resource, request
from common.SmsService import SmsService


class SendSms(Resource):
    """
    This handles the routes for sending SMSs.

    :param message(string) the message to be sent
    :param destination(string) the recipient (should be in international format)

    :return message
    """


    def post(self):
        """
        I know
        """

        # try:
        message = request.form["message"]
        destination = request.form["destination"]
        result = SmsService.send_sms(message, destination)

        # except:
        #     return {"message":"incorrect data"}

        if result:
            return {"message":"message sent"}

        else:
            return {"message":"the message was not sent"}