from flask_restful import reqparse, Resource
from common.FeedService import FeedService
from common.DatabaseService import DatabaseService
import logging as log


class Favourites(Resource):

    @staticmethod
    def get(something, user_id, page, limit=10):

        if something == 'media':
            obj_feed = FeedService(DatabaseService(), user_id, page, limit)
            dict_data = obj_feed.fetch_favourites('media')

            if "fail" in dict_data:
                return {"message": "Something went wrong on the server"}, 500
            else:
                return {"data": dict_data}

        elif something == 'all':
            obj_feed = FeedService(DatabaseService(), user_id, page, limit)
            dict_data = obj_feed.fetch_favourites('all')

            if "fail" in dict_data:
                return {"message": "Something went wrong on the server"}, 500
            else:
                return {"data": dict_data}

        else:
            return {"message": "Request resource not found"}, 404