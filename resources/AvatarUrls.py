from flask_restful import reqparse, Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService
import logging as log


class AvatarUrls(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    @staticmethod
    def get(user_ids):

        user_list = user_ids.split(',')

        try:

            for value in user_list:
                # cast each value to an integer
                value = int(value)
        except:
            log.info("one (or more) of the user_ids passed to AvatarUrls get method was not a valid int. attack????")
            return {"message": "Bad request"}, 400

        user_string = ','.join(user_list)

        bv = {"user_list": user_list}
        data = UserService(DatabaseService()).get_avatar_urls(bv, user_string)

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            log.info(data)
            return {"data": data}
