__author__ = "MArK"

# imports
from common.DatabaseService import DatabaseService
from common.CronService import CronService
from flask_restful import Resource
from time import time



class CleanUp(Resource):
    """
    Cron job to clean up temporary data in database.

    """

    def get(self):
        """
        Cleans up temporary data in the database.

        :return: 200 | 400
        """

        GRACE_PERIOD = int(time()) - 1*60*60
        # clean up tha temp_users database
        clean = CronService(DatabaseService()).clear_temporary_data(GRACE_PERIOD)

        if "fail" not in clean:
            return {"message": "clean up successful"}, 200

        else:
            return {"message": "had a problem cleaning up"}, 500