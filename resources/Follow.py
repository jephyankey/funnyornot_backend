from flask_restful import Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService
import logging as log


class Follow(Resource):

    def post(self, follower, followed):

        data = UserService(DatabaseService()).follow_({"follower": follower, "followed": followed}, 'follow')

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"message": "success"}


class UnFollow(Resource):

    def post(self, follower, followed):

        data = UserService(DatabaseService()).follow_({"follower": follower, "followed": followed}, 'unfollow')

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"message": "success"}


class Following(Resource):

    def get(self, user_id):

        data = UserService(DatabaseService()).followers_({"uid": user_id}, 'following')

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"data": data}


class Followers(Resource):

    def get(self, user_id):

        data = UserService(DatabaseService()).followers_({"uid": user_id}, 'followers')

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"data": data}


# no of followers a user has
class FollowersNo(Resource):

    def get(self, user_id):
        data = UserService(DatabaseService()).no_of_followers({"uid": user_id}, 'no-of-followers')

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"data": data[0]}


#no of ppl a user is following
class FollowingNo(Resource):

    def get(self, user_id):
        data = UserService(DatabaseService()).no_of_followers({"uid": user_id}, 'no-of-following')

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"data": data[0]}


class FollowingOrNOt(Resource):

    def get(self, follower, followed):
        data = UserService(DatabaseService()).following_or_not({"follower": follower, "followed": followed})

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            val = False if len(data) == 0 else True
            return {"data": val}
