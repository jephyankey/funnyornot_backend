from flask_restful import reqparse, Resource
from common.FeedService import FeedService
from common.DatabaseService import DatabaseService


class ImageFeed(Resource):

    @staticmethod
    def get(user_id, contact_ids, page=1, limit=10):

        contacts = contact_ids.split(',')
        obj_feed = FeedService(DatabaseService(), user_id, page, limit)
        dict_data = obj_feed.fetch_media_feed(contacts, 'images')
        if "fail" in dict_data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {'data': dict_data}

