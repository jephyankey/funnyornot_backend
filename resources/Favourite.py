from flask_restful import reqparse, Resource
from common.PostService import PostService
from common.DatabaseService import DatabaseService
import logging as log


class Favourite(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def post(self):

        self._parser.add_argument('user_id', type=int, required=True,
                                  help="You need to provide your user_id")
        self._parser.add_argument('post_id', type=int, required=True,
                                  help="You need to provide the post_id")

        args = self._parser.parse_args()
        obj_post = PostService(DatabaseService())
        data = obj_post.add_favourite(args['user_id'], args['post_id'])

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"message": "success"}

    @staticmethod
    def get(user_id, post_id):
        data = PostService(DatabaseService()).remove_favourite(user_id, post_id)

        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"message": "success"}
