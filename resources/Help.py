from flask_restful import Resource, reqparse
from common.SettingsService import SettingsService
from common.DatabaseService import DatabaseService


class Help(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()


    @staticmethod
    def get(topic_id=None):

        if topic_id:
            data = SettingsService(DatabaseService()).get_help_details(topic_id)
        else:
            data = SettingsService(DatabaseService()).get_help_topics()

        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"data": data}


    def post(self):
        self._parser.add_argument('topic', type=str, required=True)
        self._parser.add_argument('content', type=str, required=True)

        args = self._parser.parse_args()

        data = SettingsService(DatabaseService()).help_add(args['topic'], args['content'])

        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}
