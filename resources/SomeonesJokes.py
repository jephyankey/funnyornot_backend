from flask_restful import reqparse, Resource
from common.FeedService import FeedService
from common.DatabaseService import DatabaseService
import logging as log


class SomeonesJokes(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def get(self, something, user_id, someones_id, page=1, limit=10):

        if something == 'jokes':

            obj_feed = FeedService(DatabaseService(), user_id, page, limit)
            dict_data = obj_feed.get_someones_jokes(someones_id)
            if "fail" in dict_data:
                return {"message": "Something went wrong on the server"}, 500
            else:
                return {'data': dict_data}

        elif something == 'media':

            obj_feed = FeedService(DatabaseService(), user_id, page, limit)
            dict_data = obj_feed.get_someones_media(someones_id)
            if "fail" in dict_data:
                return {"message": "Something went wrong on the server"}, 500
            else:
                return {'data': dict_data}

        else:
            return {"message": "Request resource not found"}, 404