# -*- coding: UTF-8 -*-
from flask_restful import Resource
from common.ValidationService import ValidationService

class PhoneVerificationCode(Resource):
    def get(self, phone_number):
        objValidate = ValidationService()
        data = objValidate.validate_new_no(phone_number)

        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"success": True}