from flask_restful import Resource
from flask import request


class GetCountry(Resource):

    @staticmethod
    def get():
        country = request.headers.get('X-AppEngine-Country')
        return {"country": country}
