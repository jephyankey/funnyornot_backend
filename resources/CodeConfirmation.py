# -*- coding: UTF-8 -*-
from flask_restful import Resource
from common.ValidationService import ValidationService


class CodeConfirmation(Resource):
    def get(self, code, phone_number):
        objValidate = ValidationService()
        data = objValidate.confirm_verification_code(code, phone_number)

        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        elif len(data) == 0:
            return {"data": "invalid"}
        else:
            return {"data": "valid"}
