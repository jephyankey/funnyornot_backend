from flask_restful import Resource
from common.PostService import PostService
from common.DatabaseService import DatabaseService


class AbuseReport(Resource):

    @staticmethod
    def post(post_id, reporter_id):
        data = PostService(DatabaseService()).report_abuse(post_id, reporter_id)
        if "fail" in data:
            if data['fail'] == 'already done':
                return {"message": 'already done'}
            else:
                return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}
