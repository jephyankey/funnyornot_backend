from flask_restful import reqparse, Resource
from common.PostService import PostService
from common.DatabaseService import DatabaseService
import logging as log


class BlockPost(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()


    def get(self, user_id):

        data = PostService(DatabaseService()).unblock_all_posts(user_id)
        if "fail" in data:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"message": "success"}


    def post(self):

        self._parser.add_argument('user_id', type=int, required=True,
                                  help="You need to provide your user_id")
        self._parser.add_argument('post_id', type=int, required=True,
                                  help="You need to provide the post_id")

        args = self._parser.parse_args()
        obj_post = PostService(DatabaseService())
        data = obj_post.block_post(args['user_id'], args['post_id'])

        if len(data) == 0:
            return {"message": "Internal Server Error"}, 500
        else:
            return {"data": data}