from flask_restful import Resource
from common.SettingsService import SettingsService
from common.DatabaseService import DatabaseService


class InvisibilitySett(Resource):

    @staticmethod
    def post(user_id, value):

        if value != 1 and value != 0:
            return {"message": "Bad Request, val not val"}, 400

        data = SettingsService(DatabaseService()).set_visibility(user_id, value)

        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}
