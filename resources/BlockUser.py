from flask_restful import Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService
import logging as log


class BlockUser(Resource):

    def post(self, user_id, blocked_id):
        obj_user = UserService(DatabaseService())
        bound_values = {"user_id": user_id, "blocked_id": blocked_id}
        data = obj_user.block_user(bound_values)
        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}


class UnblockUser(Resource):

    def post(self, user_id, blocked_id):
        obj_user = UserService(DatabaseService())
        bound_values = {"user_id": user_id, "blocked_id": blocked_id}
        data = obj_user.unblock_user(bound_values)
        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}