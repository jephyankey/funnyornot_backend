__author__ = "MArK"

from flask_restful import Resource, request
from common.DatabaseService import DatabaseService

from common.UserService import UserService
from common.CloudService import CloudService
import logging as log

class Upload(Resource):
    def post(self):
        """
         save a file on the cloud storage (bucket) and returns its public url.

         :param uploaded_file(file) the file to be saved
         :param filename_to_use(string) the filename of the file


         :returns public_url, (file_name) | message
         """


        # get the file
        try:
            uploaded_file = request.files["file"]
            filename_to_use = request.form.get('filename')


        except:
            return {"message": "incorrect data"}, 400


        # check if this is an avatar upload
        if request.form.get('is_avatar') and request.form.get("user_id"):
            user_id = request.form["user_id"]
            # delete old avatar
            CloudService.delete_avatar(user_id)
            # save image file and return the public url
            public_url, file_name, poster_url = CloudService.save_file(uploaded_file, filename_to_use,
                                                                               True, None)

            # now update database with new avatar url
            update = UserService(DatabaseService()).update_avatar_url(public_url, file_name, user_id)

            if "fail" not in update and public_url and file_name:
                return {"url": public_url, "filename": file_name}, 200
            else:
                return {"message": "Something wrong with file upload/avatar url update"}, 500

        else:

            # check if we have a poster image (should come with videos)
            if request.form.get("poster_image"):
                poster_image = request.form["poster_image"]
            else:
                poster_image = None

            # save (video | audio | image) file and return the public url
            public_url, file_name, poster_url = CloudService.save_file(uploaded_file, filename_to_use,
                                                                           None, poster_image)

            if public_url and file_name:
                return {"url": public_url, "filename": file_name, "poster_url": poster_url}, 200
            else:
                return {"message": "Wrong File"}, 400




    def delete(self):
        """
         delete a file

         :param file_name(string) the name of the file to be deleted

         :return Message
         """

        # get the url
        try:
            image_name = request.args["image_name"]

        except:
            return {"message": "incorrect data"}, 400

        # delete the image
        if CloudService.delete_file(image_name):
            return {"message": "file deleted"}, 200

        else:
            return {"message": "Image does not exist"}, 200