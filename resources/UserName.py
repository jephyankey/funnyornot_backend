from flask_restful import Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService


class UserName(Resource):

    @staticmethod
    def post(user_id, username):
        data = UserService(DatabaseService()).update_username(user_id, username)
        if "fail" in data:
            if data['fail'] == 'unavailable':
                return {"message": "unavailable"}
            else:
                return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}
