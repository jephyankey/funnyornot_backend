# -*- coding: UTF-8 -*-
from flask_restful import Resource
from common.FeedService import FeedService
from common.DatabaseService import DatabaseService
# import logging as log
# import json


class Rankings(Resource):

    def get(self, filter_, country_code, user_id, page=1, limit=15):
        obj_dbase = DatabaseService();
        obj_feed = FeedService(obj_dbase, '', page, limit)

        if filter_ == 'alltime':
            dict_data = obj_feed.rank(country_code, user_id, 'rank-alltime')
            if "fail" in dict_data:
                return {"message": "Something went wrong on the server"}, 500
            else:
                return {"data": dict_data}

        elif filter_ == 'recently':
            dict_data = obj_feed.rank(country_code, user_id, 'rank-today')
            if "fail" in dict_data:
                return {"message": "Something went wrong on the server"}, 500
            else:
                return {"data": dict_data}

        else:
            return {"message": "Request resource not found"}, 404
