from flask_restful import reqparse, Resource
from common.UserService  import UserService
from common.DatabaseService import DatabaseService
import logging as log


class NewUser(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def post(self):
        self._parser.add_argument('username', type=str, required=True)
        self._parser.add_argument('phone_number', type=str, required=True)
        self._parser.add_argument('country_SN', type=str, required=True)
        self._parser.add_argument('device_id', type=str, required=True)

        args = self._parser.parse_args()
        bv = {"username": args['username'], "contact": args['phone_number'], "country_code": args['country_SN'], "device_id": args['device_id']}
        obj_user = UserService(DatabaseService())
        data = obj_user.create_user(bv)

        if "fail" in data:
            if data["fail"] == 'unavailable':
                return {"data": "unavailable"}
            else:
                return {"message": "Something went wrong on the server"}, 500
        else:
            return {"data": data['user_id']}


