from flask_restful import Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService


class DeviceId(Resource):

    @staticmethod
    def post(user_id, device_id):
        data = UserService(DatabaseService()).update_device_id(user_id, device_id)
        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {"message": "success"}
