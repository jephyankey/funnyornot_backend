from flask_restful import reqparse, Resource
from common.PostService import PostService
from common.DatabaseService import DatabaseService
import logging as log


class NewPost(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def post(self, user_id):
        self._parser.add_argument('media_type', type=str, required=True)
        self._parser.add_argument('post_text', type=str, required=False)
        self._parser.add_argument('media_url', type=str, required=False)
        self._parser.add_argument('filename', type=str, required=False)
        self._parser.add_argument('poster_image', type=str, required=False)

        args = self._parser.parse_args()

        media_type = args['media_type']
        text = args['post_text'] if args["post_text"] else None
        media_url = args['media_url'] if args["media_url"] else None
        filename = args['filename'] if args["filename"] else None
        poster_image = args['poster_image'] if args["poster_image"] else None

        # text = text if text else None
        # media_url = media_url if media_url else None

        if text is None and media_url is None:
            return {"message": "No text, No media, How did this happen"}, 500

        obj_post = PostService(DatabaseService())
        data = obj_post.create_post(user_id, text, media_type, media_url, filename, poster_image)
        if "fail" in data:
            return {"message": "Something went wrong on the server"}, 500
        else:
            return {'data': data}
