from flask_restful import reqparse, Resource
from common.UserService import UserService
from common.DatabaseService import DatabaseService
import logging as log
import phonenumbers
from urllib import unquote


class Friends(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()

    def post(self):
        self._parser.add_argument('contact_list', type=str, required=True, help="Provide a list of contacts")
        args = self._parser.parse_args()

        contacts = unquote(args['contact_list'])
        contacts_array = contacts.split(',')
        international_contacts = []

        # reformat phone numbers into international format where necessary
        for contact in contacts_array:
            try:
                contact = phonenumbers.parse(contact, 'GH')
                international_contacts.append(phonenumbers.format_number(contact, phonenumbers.PhoneNumberFormat.E164))
            except Exception as e:
                international_contacts.append("xxx-1j1-error")
                log.info('Some number is not really a number. Not my problem!')
        obj_user = UserService(DatabaseService())
        bv = {"contacts": international_contacts}
        data = obj_user.find_friends(bv)

        # todo code below is potentially computationally expensive, revise
        # find the position of each contact (i.e. pos in the original array from the client)
        # which was found in the database
        final_list = []
        all_ids = []
        all_device_ids = []
        for user in data:
            pos = 0
            for contact in international_contacts:
                if contact == user["contact"]:
                    final_list.append([pos, user["user_id"]])
                    all_ids.append(user["user_id"])
                    all_device_ids.append(user["device_id"])
                    # todo: test the following two lines instead:
                    # pos += 1
                    # break?
                pos += 1

        return {"data": final_list, "all_ids": all_ids, "all_device_ids": all_device_ids}
