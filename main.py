# -*- coding: UTF-8 -*-
from flask import Flask
from flask_restful import Api
from resources.NewUser import NewUser
from resources.HomeFeed import Feed
from resources.HelloWorld import HelloWorld
from resources.Explore import Explore
from resources.NewPost import NewPost
from resources.Friends import Friends
from resources.Rankings import Rankings
from resources.BlockPost import BlockPost
from resources.FunFactor import FunFactor
from resources.GetCountry import GetCountry
from resources.Favourites import Favourites
from resources.Favourite import Favourite
from resources.SetFunnyOrNot import SetFunnyOrNot
from resources.SomeonesJokes import SomeonesJokes
from resources.CleanUp import CleanUp
from resources.DeviceId import DeviceId

from resources.Follow import *
from resources.BlockUser import *
from resources.AvatarUrls import AvatarUrls

from resources.Upload import Upload
from resources.SendSms import SendSms
from resources.UserName import UserName
from resources.ImageFeed import ImageFeed
from resources.VideoFeed import VideoFeed
from resources.AbuseReport import AbuseReport

from resources.Help import Help
from resources.InvisibilitySett import InvisibilitySett
from resources.WhoSeesDpSett import WhoSeesDpSett

from resources.CodeConfirmation import CodeConfirmation
from resources.PhoneVerificationCode import PhoneVerificationCode

app = Flask(__name__)
api = Api(app)


# ----------- define all routes------------------------------------------------------------------
api.add_resource(HelloWorld, '/', endpoint='hello')

api.add_resource(GetCountry, '/myCountry/', endpoint="country")

api.add_resource(Feed,
                 '/feed/<int:user_id>/<string:contact_ids>',
                 '/feed/<int:user_id>/<string:contact_ids>/<int:page>',
                 '/feed/<int:user_id>/<string:contact_ids>/<int:page>/<int:limit>',
                 endpoint='feed')

api.add_resource(ImageFeed,
                 '/imageFeed/<int:user_id>/<string:contact_ids>',
                 '/imageFeed/<int:user_id>/<string:contact_ids>/<int:page>',
                 '/imageFeed/<int:user_id>/<string:contact_ids>/<int:page>/<int:limit>',
                 endpoint='image_feed')

api.add_resource(VideoFeed,
                 '/videoFeed/<int:user_id>/<string:contact_ids>',
                 '/videoFeed/<int:user_id>/<string:contact_ids>/<int:page>',
                 '/videoFeed/<int:user_id>/<string:contact_ids>/<int:page>/<int:limit>',
                 endpoint='video_feed')

api.add_resource(Explore,
                 '/explore/<string:affiliate_countries>/<int:user_id>/<string:contact_ids>',
                 '/explore/<string:affiliate_countries>/<int:user_id>/<string:contact_ids>/<int:page>',
                 '/explore/<string:affiliate_countries>/<int:user_id>/<string:contact_ids>/<int:page>/<int:limit>',
                 endpoint='explore')

api.add_resource(Rankings,
                 '/trending/<string:filter_>/<string:country_code>/<int:user_id>/<int:page>',
                 '/trending/<string:filter_>/<string:country_code>/<int:user_id>/<int:page>/<int:limit>',
                 endpoint='rankings')

api.add_resource(NewPost,
                 '/post/<int:user_id>',
                 endpoint='new_post')

api.add_resource(Friends,
                 '/findFriends',
                 endpoint='findFriends')

api.add_resource(Favourites,
                 '/favourites/<string:something>/<int:user_id>',
                 '/favourites/<string:something>/<int:user_id>/<int:page>',
                 '/favourites/<string:something>/<int:user_id>/<int:page>/<int:limit>',
                 endpoint='favourites')

api.add_resource(SetFunnyOrNot,
                 '/setAsFunny',
                 endpoint='set_as_funny_or_not')

api.add_resource(SomeonesJokes,
                 '/someone/<string:something>/<int:user_id>/<int:someones_id>/<int:page>',
                 '/someone/<string:something>/<int:user_id>/<int:someones_id>/<int:page>/<int:limit>',
                 endpoint='someones_jokes')

api.add_resource(BlockPost,
                 '/blockPost',
                 endpoint='block_post')

api.add_resource(BlockPost,
                 '/blockReset/<int:user_id>',
                 endpoint='reset_blocked_posts')

api.add_resource(Favourite,
                 '/favourite',
                 '/favourite/delete/<int:user_id>/<int:post_id>', # temp cos DELETE verb is not working
                 endpoint='add_rem_favourite')

api.add_resource(Upload,
                 '/upload',
                 endpoint='upload')

api.add_resource(AbuseReport,
                 '/abuseReport/<int:post_id>/<int:reporter_id>',
                 endpoint='report_abuse')

api.add_resource(SendSms,
                 '/sendsms',
                 endpoint='sendsms')

api.add_resource(CleanUp,
                 '/cleanup',
                 endpoint='cleanup')


# ------------------------------------user operations ---------------------------------------

api.add_resource(UserName,
                 '/userName/<int:user_id>/<string:username>',
                 endpoint='update_username')

api.add_resource(DeviceId,
                 '/deviceId/<int:user_id>/<string:device_id>',
                 endpoint='update_device_id')

api.add_resource(BlockUser,
                 '/blockUser/<int:user_id>/<int:blocked_id>',
                 endpoint='blockUser')

api.add_resource(UnblockUser,
                 '/unblockUser/<int:user_id>/<int:blocked_id>',
                 endpoint='unblockUser')

api.add_resource(FunFactor,
                 '/funFactor/<int:user_id>',
                 endpoint='fun_factor')


api.add_resource(AvatarUrls, '/avatarUrls/<string:user_ids>', endpoint="av_urls")

# ------------------------------------following and related----------------------------------
api.add_resource(Follow,
                 '/follow/<int:follower>/<int:followed>',
                 endpoint='follow')

api.add_resource(UnFollow,
                 '/unfollow/<int:follower>/<int:followed>',
                 endpoint='unfollow')

api.add_resource(Following,
                 '/following/<int:user_id>',
                 endpoint='following')

api.add_resource(Followers,
                 '/followers/<int:user_id>',
                 endpoint='followers')

api.add_resource(FollowersNo,
                 '/numberFollowers/<int:user_id>',
                 endpoint='no_of_followers')

api.add_resource(FollowingNo,
                 '/numberFollowing/<int:user_id>',
                 endpoint='no_user_is_following')

api.add_resource(FollowingOrNOt,
                 '/followingOrNot/<int:follower>/<int:followed>',
                 endpoint='following_or_not')


# ---------------------------- settings related routes --------------------------------------
api.add_resource(Help,
                 '/help/<int:topic_id>',  # get to this gives topic content
                 '/help',                   # get gives all topics, post adds new topic
                 endpoint='help')

api.add_resource(InvisibilitySett,
                 '/settings/invisible/<int:user_id>/<int:value>',
                 endpoint='update_visibility')

api.add_resource(WhoSeesDpSett,
                 '/settings/whoSeesDp/<int:user_id>/<int:value>',
                 endpoint='who_sees_dp')


# --------------------verification and security related routes--------------------------------

api.add_resource(PhoneVerificationCode,
                 '/phoneVerifyCode/<string:phone_number>',
                 endpoint='send_verification_code')

api.add_resource(CodeConfirmation,
                 '/codeConfirmation/<string:code>/<string:phone_number>',
                 endpoint='confirm_verify_code')

api.add_resource(NewUser,
                 '/user/',
                 endpoint='create_user')


# ----------- other configurations-------------------------------------------------------------

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE')
    response.headers.add('Access-Control-Allow-Headers', 'content-type')
    return response


# ----------- run main app ----------------------------------------------------------------------
if __name__ == '__main__':
    app.run(debug=True)

# todo REMOVE FK in mysql for more speed
# todo in app, remove connect error page from nav stack, if there is a network error, and then a succesful connection,
# otherwise the phones back btn will bring u back there

# todo change flask-restful's default error messages to json format (currently html)