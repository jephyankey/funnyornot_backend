__author__="MArK"

# todo audio, video and image quality
# todo look into 320 pixels and resizing smaller images

from google.appengine.api import images
from google.appengine.api import blobstore
import cloudstorage
import imghdr
import logging as log
from base64 import b64decode
import os
from google.appengine.api import app_identity
from PIL import ImageFilter, Image
from common.DatabaseService import DatabaseService
from common.UserService import UserService


from werkzeug.utils import secure_filename

# the bucket nameS for various media
# REMEMBER: the main bucket is used for images and a different public bucket
# is used for other media
IMAGES_BUCKET = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())
PUBLIC_BUCKET = "rt4d1qc4zt7xb9di5j6sjjvjkbndyi8b93n3yusj0rxeuqlg121zlw7iu71nhew"
VIDEO_FOLDER = "eeixeqsxpygy9m70x1wv7iuktcwpVID"
AUDIO_FOLDER = "i8xsf5497m1yensckeocnoau3mwAUD"


class CloudService():
    @staticmethod
    def get_file_url(file_location, content_type, file_name):
        """
         get the public url for the file.

         :param file_location(string): the location of the file
         :param content_type(string): the content type of the file
         :param file_name(string): the file name generated for the file

         :return public_url
         """

        # get the public url
        if "image/" in content_type:
            # get the blob key
            blob_key = CloudService.get_blob_key(file_location)
            public_url = images.get_serving_url(blob_key)

        elif "audio/" in content_type:
            public_url = "https://{}.storage.googleapis.com/{}/{}".format(PUBLIC_BUCKET, AUDIO_FOLDER, file_name)

        elif "video/" in content_type:
            public_url = "https://{}.storage.googleapis.com/{}/{}".format(PUBLIC_BUCKET, VIDEO_FOLDER, file_name)

        return public_url



    @staticmethod
    def save_file(the_file, filename_to_use, is_avatar, poster_image):
        """
         save a file on the cloud storage (bucket) and provides its public url.

         :param the_file(file): the file to be saved
         :param filename_to_use the name to save the file with as sent from frontend
         :param is_avatar: the type of image file
         :param poster_image is the poster image for a video upload

         :return public_url, file_name | False, False
         """

        # get the start sting, format and content type
        content_type, form, start = CloudService.get_file_type(the_file, is_avatar)

        public_url_poster = ""  # assume there is no poster, will be overwritten for videos only

        if content_type and form and start:

            try:
                file_name = secure_filename("_".join([start, filename_to_use[:filename_to_use.index(".")]]))

            except ValueError:
                file_name = secure_filename("_".join([start, filename_to_use]))

            file_name = file_name + "_"
            file_name = ".".join([file_name, form])

            if "audio/" in content_type:
                bucket = PUBLIC_BUCKET + "/" + AUDIO_FOLDER
                file_location = "/" + bucket + "/" + file_name
                # open a new file for writing
                filen = cloudstorage.open(file_location, "w", content_type, options={'x-goog-acl': 'public_read'})

            elif "video/" in content_type:
                # first work on the poster image
                poster_image = poster_image[23:]
                poster_image_name = 'poster_'+secure_filename(filename_to_use)+'_.png'
                # convert poster image from base64 and save to cloudstorage
                file_loc_poster = "/" + IMAGES_BUCKET + "/" + poster_image_name

                a = cloudstorage.open(file_loc_poster, "w", "image/jpg")
                a.write(b64decode(poster_image))
                a.close()
                public_url_poster = CloudService.get_file_url(file_loc_poster, "image/jpg", poster_image_name)

                bucket = PUBLIC_BUCKET + "/" + VIDEO_FOLDER
                file_location = "/" + bucket + "/" + file_name
                # open a new file for writing
                filen = cloudstorage.open(file_location, "w", content_type, options={'x-goog-acl': 'public_read'})

            else:
                file_location = "/" + IMAGES_BUCKET + "/" + file_name
                filen = cloudstorage.open(file_location, "w", content_type)  # open a new file for writing
                if not is_avatar and "image/gif" not in content_type:
                    # apply image enhancement filter
                    image = Image.open(the_file)
                    the_file = image.filter(ImageFilter.UnsharpMask(1, 100, 3))

            the_file.save(filen)
            filen.close()

            # obtain media serving url
            public_url = CloudService.get_file_url(file_location, content_type, file_name)

            # return the public url
            return public_url, file_name, public_url_poster

        else:
            return False, False, False



    @staticmethod
    def delete_avatar(user_id):
        """
        Delete an avatar (image).

        :param user_id(int): the id of the user whose avatar is to be deleted

        """
        # get the file location for the avatar
        file_name = UserService(DatabaseService()).get_avatar_filename(user_id)

        if file_name is not None:
            file_location = "/" + IMAGES_BUCKET + "/" + file_name

            if file_location:
                CloudService.delete_file(file_location)



    @staticmethod
    def delete_file(file_location):
        """
         Delete an image.

         :param file_location(string): the file name of the file to be deleted

         :returns: True | False
         """

        # check the file's availability
        if cloudstorage.cloudstorage_api._file_exists(file_location):
            # obtain blob key for file
            blob_key = CloudService.get_blob_key(file_location)

            # delete file
            cloudstorage.delete(file_location)

            # delete public url
            blobstore.delete(blob_key)

            return True

        else:
            return False



    @staticmethod
    def get_blob_key(file_location):
        """
        get the blob key for a particular file.

        :param file_location(string) the name of the file

        :return blob_key
        """

        # get the key
        blob_name = "/gs" + file_location
        blob_key = blobstore.create_gs_key(blob_name)
        return blob_key




    @staticmethod
    def get_file_type(fil, is_avatar):
        """
        get th content type and accompanying bucket for a particular file.

        :param fil(file) the file in question
        :param is_avatar(boolean) the type of image file

        :returns content_type, form, start, bucket
        """

        if is_avatar:
            # image
            if "image/" in fil.content_type:
                form = imghdr.what(fil)
                if form is not None:
                    content_type = "image/" + form
                    start = "avatar"
                    # bucket = IMAGES_BUCKET

                else:
                    content_type = None
                    start = None
                    bucket = None
                    log.info("This image is not an image")

        elif is_avatar is None:
            # image
            if "image/" in fil.content_type:
                form = imghdr.what(fil)
                if form is not None:
                    content_type = "image/" + form
                    start = "image"
                    # bucket = IMAGES_BUCKET

                else:
                    content_type = None
                    start = None
                    # bucket = None
                    log.info("This image is not an image")

            # audio
            elif "audio/" in fil.content_type:
                content_type = fil.content_type
                form = fil.content_type[fil.content_type.index("/") + 1:]
                start = "aud"
                # bucket = PUBLIC_BUCKET

            # video
            elif "video/" in fil.content_type:
                content_type = fil.content_type
                form = fil.content_type[fil.content_type.index("/") + 1:]
                start = "vid"
                # bucket = PUBLIC_BUCKET

        else:
            content_type = form = start = None

        return content_type, form, start



    @staticmethod
    def get_frame_from_video(fil):
        """
        Gets one random frame from a video file.

        :param fil(file) a video file

        :return frame
        """

        # get a lis of all frames in the video then pick one and return it
        # ff = ffmpy.FFmpeg(inputs={fil: None},outputs={'image.jpg': None})
        # ff.run()
        # frame = ff
        #
        # return frame

    # @staticmethod
    # def decode_base64(data):
    #     """Decode base64, padding being optional.
    #
    #     :param data: Base64 data as an ASCII byte string
    #     :returns: The decoded byte string.
    #
    #     """
    #     missing_padding = len(data) % 4
    #     if missing_padding != 0:
    #         data += b'=' * (4 - missing_padding)
    #         log.info('wrong padding................')
    #     return b64decode(data)


    # todo work on
    @staticmethod
    def compress_image(the_file_to_be_compressed, file_location, content_type):
        """

        Compresses an image file.

        :param the_file_to_be_compressed: the image to compress
        :param file_location: the location of the file
        :param content_type: the content type of the image

        :return: compressed file
        """

        im = Image.open(the_file_to_be_compressed)
        width = im.width
        blob_key = CloudService.get_blob_key(file_location)
        image = images.Image(blob_key=blob_key)
        image.resize(width=width)
        if "image/png" in content_type:
            compressed_file = image.execute_transforms(output_encoding=images.PNG, quality=90)

        if "image/jpeg" in content_type:
            compressed_file = image.execute_transforms(output_encoding=images.JPEG, quality=90)

        if "image/gif" in content_type:
            compressed_file = image.execute_transforms(output_encoding=images.GIF, quality=90)

        return compressed_file
