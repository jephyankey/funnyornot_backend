# -*- coding: UTF-8 -*-
import logging as log
from common.Utilities import Utilities


class FeedService:

    def __init__(self, db_service, user_id, page, limit):
        self._db_service = db_service
        self._limit = limit
        self._offset = (page-1)*self._limit
        # ^pages are numbered from 1,2,3....etc. Hence for first page, api will receive page 1,
        # however since the offset as used in the database starts from 0, page-1
        self._bv = {"lim": self._limit, "off": self._offset, "uid": user_id}


    def fetch_feed(self, ids_of_contacts):
        self._bv['contacts'] = ids_of_contacts
        prepared_query = self.write_query()
        try:
            data = self._db_service.run_prepared_query(prepared_query, self._bv)
            for one in data:
                one['how_long_ago'] = Utilities.timesince(one['date_posted'])
            return data
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def fetch_media_feed(self, ids_of_contacts, media_type):
        self._bv['contacts'] = ids_of_contacts
        prepared_query = self.write_query('feed-images') if media_type == 'images' else self.write_query('feed-videos')
        try:
            data = self._db_service.run_prepared_query(prepared_query, self._bv)
            for one in data:
                one['how_long_ago'] = Utilities.timesince(one['date_posted'])
            return data
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def explore(self, countries, ids_of_contacts):
        self._bv["cid"] = countries
        self._bv['contacts'] = ids_of_contacts

        prepared_query = self.write_query('explore-feed')
        try:
            data = self._db_service.run_prepared_query(prepared_query, self._bv)
            for one in data:
                one['how_long_ago'] = Utilities.timesince(one['date_posted'])
            return data
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    # rank_filter should be either rank-alltime or rank-today
    def rank(self, country_code, user_id, rank_filter):
        self._bv["cid"] = country_code
        self._bv["uid"] = user_id
        prepared_query = self.write_query(rank_filter)
        try:
            data = self._db_service.run_prepared_query(prepared_query, self._bv)
            for one in data:
                one['how_long_ago'] = Utilities.timesince(one['date_posted'])
            return data
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def get_someones_jokes(self, someones_id):
        self._bv["someones_id"] = someones_id
        try:
            return self._db_service.run_prepared_query(self.write_query('someones-jokes'), self._bv)
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def get_someones_media(self, someones_id):
        self._bv["someones_id"] = someones_id
        try:
            return self._db_service.run_prepared_query(self.write_query('someones-media'), self._bv)
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def fetch_favourites(self, mode):
        try:
            if mode == 'all':
                return self._db_service.run_prepared_query(self.write_query('favourites'), self._bv)
            elif mode == 'media':
                return self._db_service.run_prepared_query(self.write_query('favourites-media'), self._bv)
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    # gracefully handle errors generated in this class
    def handle_errors(self, e):
        log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))
        return


    # function returns appropriate query based on type of feed
    @staticmethod
    def write_query(mode='regular-feed'):
        if mode == 'regular-feed':
            return \
                '''
                    SELECT OP.date_posted, OP.poster_id, OP.parent, PB.post_id, OU.username, post_text,
                            P.reposted, U.username AS reposter_name, U.user_id as reposter_id,
                            media_url, funny, not_funny, media_type, stars, OU.avatar_url, OU.avatar_filename, filename,
                            poster_image, 'home' AS feed, UF.post_id AS favourited
                    FROM posts P
                    JOIN
                    (
                        # MIN(post_id): ensures that for a post and all its reposts, only the original post is shown
                        SELECT MIN(post_id) AS post_id FROM posts
                          WHERE
                              (
                                  poster_id IN %(contacts)s
                                  OR poster_id IN (SELECT followed FROM user_follows_user WHERE follower = %(uid)s)
                                  OR poster_id = (%(uid)s)
                              )
                          # grouping by parent prevents reposts and original posts from showing up in same feed
                          GROUP BY parent
                    ) S
                          ON P.post_id = S.post_id

                    JOIN post_body PB ON P.parent = PB.post_id

                    # get the details of the poster (not necessarily the OP, could be the reposter)
                    JOIN users U ON P.poster_id = U.user_id

                    # if this is a repost, the line below will get the original poster, otherwise u are just getting
                    # the same as already gotten above  (OP  -- for original poster, OU -- original user)
                    JOIN posts OP ON OP.parent = PB.post_id AND OP.reposted = 0
                    JOIN users OU ON OP.poster_id = OU.user_id

                    LEFT JOIN user_stars_post UP ON UP.post_id = PB.post_id AND UP.user_id = (%(uid)s)
                    LEFT JOIN user_has_favs UF ON UF.post_id = PB.post_id AND UF.user_id = (%(uid)s)

                    WHERE PB.post_id NOT IN (SELECT post_id FROM user_blocks_post WHERE user_id = %(uid)s)
                    AND P.poster_id NOT IN (SELECT blocked_id FROM user_blocks_user WHERE user_id = (%(uid)s))

                    ORDER BY date_posted DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'feed-images':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id, post_text,
                            media_url, funny, not_funny, media_type, filename
                    FROM posts P
                    JOIN
                    (
                        # MIN(post_id): ensures that for a post and all its reposts, only the original post is shown
                        SELECT MIN(post_id) AS post_id FROM posts
                          WHERE (
                                  poster_id IN %(contacts)s
                                  # -- LINE BELOW COMMENTED OUT COS FOR NOW, the feature of following is not implemented
                                  OR poster_id IN (SELECT followed FROM user_follows_user WHERE follower = %(uid)s)
                                  OR poster_id = (%(uid)s)
                                 )

                          # GET IMAGES ONLY
                          # grouping by parent prevents reposts and original posts from showing up in same feed
                          GROUP BY parent
                    ) S
                          ON P.post_id = S.post_id
                    JOIN post_body PB ON P.parent = PB.post_id
                    WHERE media_type = 'i'

                    AND poster_id NOT IN (SELECT blocked_id FROM user_blocks_user WHERE user_id = (%(uid)s))
                    AND PB.post_id NOT IN (SELECT post_id FROM user_blocks_post WHERE user_id = %(uid)s)
                    # JOIN users U ON P.poster_id = U.user_id
                    # LEFT JOIN user_stars_post UP ON UP.post_id = PB.post_id AND UP.user_id = %(uid)s
                    ORDER BY date_posted DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'feed-videos':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id,  post_text,
                            media_url, funny, not_funny, media_type, filename, poster_image,
                            poster_image as cached_media_url
                    FROM posts P
                    JOIN
                    (
                        # MIN(post_id): ensures that for a post and all its reposts, only the original post is shown
                        SELECT MIN(post_id) AS post_id FROM posts
                          WHERE (
                                  poster_id IN %(contacts)s
                                  # -- LINE BELOW COMMENTED OUT COS FOR NOW, the feature of following is not implemented
                                  OR poster_id IN (SELECT followed FROM user_follows_user WHERE follower = %(uid)s)
                                  OR poster_id = (%(uid)s)
                                )
                          # grouping by parent prevents reposts and original posts from showing up in same feed
                          GROUP BY parent
                    ) S
                          ON P.post_id = S.post_id
                    JOIN post_body PB ON P.parent = PB.post_id
                    WHERE media_type = 'v'

                    AND poster_id NOT IN (SELECT blocked_id FROM user_blocks_user WHERE user_id = (%(uid)s))
                    AND PB.post_id NOT IN (SELECT post_id FROM user_blocks_post WHERE user_id = %(uid)s)

                    # JOIN users U ON P.poster_id = U.user_id
                    # LEFT JOIN user_stars_post UP ON UP.post_id = PB.post_id AND UP.user_id = %(uid)s
                    ORDER BY date_posted DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'explore-feed':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id, username, post_text,
                            media_url, funny, not_funny, media_type, stars, avatar_filename, filename, poster_image,
                            'explore' as feed, UF.post_id AS favourited,
                            IF(who_sees_dp = 0, avatar_url, NULL) as avatar_url

                    FROM post_body PB
                    JOIN posts P ON PB.post_id = P.parent
                    JOIN users U ON P.poster_id = U.user_id
                    LEFT JOIN user_stars_post UP ON UP.post_id = PB.post_id AND UP.user_id = (%(uid)s)
                    LEFT JOIN user_has_favs UF ON UF.post_id = PB.post_id AND UF.user_id = (%(uid)s)

                    WHERE country_code IN %(cid)s
                    AND reposted = 0
                    # AND poster_id <> (%(uid)s)  # taken care of in last condition
                    AND poster_id NOT IN (SELECT followed FROM user_follows_user WHERE follower = %(uid)s)
                    AND poster_id NOT IN (SELECT blocked_id FROM user_blocks_user WHERE user_id = (%(uid)s))
                    AND PB.post_id NOT IN ( SELECT UB.post_id FROM user_blocks_post UB WHERE user_id = (%(uid)s))
                    # -- weed out posts  made by ppl in your contact list (this will appear in your feed)
                    # -- also weeds out posts that have been reposted by ppl in my contact list
                    # -- also weeds out posts that were posted/reposted by current user
                    AND PB.post_id NOT IN (SELECT parent FROM posts WHERE poster_id IN %(contacts)s OR poster_id = %(uid)s)
                                                                    # or poster_id in (select blocked list)
                    ORDER BY date_posted DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        # everything posted by owner(e.g. owner of profile page) which has not been blocked by current user
        elif mode == 'someones-jokes':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id, username, post_text,
                            media_url, funny, not_funny, media_type, stars, avatar_url, avatar_filename,
                             filename, poster_image, UF.post_id AS favourited
                    FROM post_body PB
                    JOIN posts P ON P.parent = PB.post_id
                    JOIN users U ON P.poster_id = U.user_id
                    LEFT JOIN user_stars_post UL ON PB.post_id = UL.post_id AND UL.user_id = (%(uid)s)
                    LEFT JOIN user_has_favs UF ON UF.post_id = PB.post_id AND UF.user_id = (%(uid)s)

                    WHERE poster_id = (%(someones_id)s)
                      # u cant see someones jokes if u have blocked him or he has blocked u.
                      AND NOT EXISTS
                        (SELECT 1 from user_blocks_user WHERE (user_id = (%(uid)s) and blocked_id = (%(someones_id)s))
                          or (user_id = (%(someones_id)s) and blocked_id = (%(uid)s) ))

                      AND reposted = 0  # ensure its an original post
                      AND PB.post_id NOT IN (SELECT post_id FROM user_blocks_post WHERE user_id = (%(uid)s))
                    ORDER BY date_posted DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''
        # every media (video, img, sound) posted by owner(e.g. owner of profile page)
        # which has not been blocked by current user
        # TODO add an index to media_url column in database
        elif mode == 'someones-media':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id, username, post_text,
                            media_url, funny, not_funny, media_type, stars, avatar_url, avatar_filename,
                            filename, poster_image, poster_image as cached_media_url
                    FROM post_body PB
                    JOIN posts P ON P.parent = PB.post_id
                    JOIN users U ON P.poster_id = U.user_id
                    LEFT JOIN user_stars_post UL ON PB.post_id = UL.post_id AND UL.user_id = (%(uid)s)
                    WHERE poster_id = (%(someones_id)s)
                    # u cant see someones media if u have blocked him or he has blocked u.
                      AND NOT EXISTS
                        (SELECT 1 from user_blocks_user WHERE (user_id = (%(uid)s) and blocked_id = (%(someones_id)s))
                          or (user_id = (%(someones_id)s) and blocked_id = (%(uid)s) ))

                      AND reposted = 0 # ensure its an original post
                      AND media_url IS NOT NULL
                      AND PB.post_id NOT IN ( SELECT UB.post_id FROM user_blocks_post UB WHERE user_id = (%(uid)s))
                      ORDER BY date_posted DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'rank-alltime':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id, username, post_text,
                            media_url, funny, not_funny, media_type, stars, avatar_url, avatar_filename,
                            filename, poster_image
                    FROM post_body PB
                    JOIN posts P ON P.parent = PB.post_id
                    JOIN users U ON P.poster_id = U.user_id
                    LEFT JOIN user_stars_post UL ON PB.post_id = UL.post_id and UL.user_id = (%(uid)s)
                    WHERE country_code = (%(cid)s)
                      AND reposted = 0 # only original posts
                      AND funny-not_funny > 10
                      AND poster_id NOT IN (SELECT blocked_id FROM user_blocks_user WHERE user_id = (%(uid)s))
                      AND PB.post_id NOT IN ( SELECT UB.post_id FROM user_blocks_post UB WHERE user_id = (%(uid)s))
                    ORDER BY funny-not_funny DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'rank-today':
            return \
                '''
                    SELECT date_posted, poster_id, parent, reposted, PB.post_id, username, post_text,
                            media_url, funny, not_funny, media_type, stars, avatar_url, avatar_filename,
                            filename, poster_image
                    FROM post_body PB
                    JOIN posts P ON P.parent = PB.post_id
                    JOIN users U ON P.poster_id = U.user_id
                    LEFT JOIN user_stars_post UL ON PB.post_id = UL.post_id and UL.user_id = (%(uid)s)
                    WHERE country_code = (%(cid)s)
                      AND reposted = 0 # only original posts
                      AND funny-not_funny > 10
                      AND date_posted > UNIX_TIMESTAMP() - 3*24*3600  # 3 DAYS
                      AND poster_id NOT IN (SELECT blocked_id FROM user_blocks_user WHERE user_id = (%(uid)s))
                      AND PB.post_id NOT IN ( SELECT UB.post_id FROM user_blocks_post UB WHERE user_id = (%(uid)s))
                    ORDER BY funny-not_funny DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'favourites':
            return \
                '''
                    SELECT PB.post_id, date_posted, poster_id, parent, reposted, post_text,
                            media_url, funny, not_funny, media_type, stars, filename, poster_image,
                            true as favourited
                    FROM user_has_favs UF
                    JOIN post_body PB ON PB.post_id = UF.post_id
                    JOIN posts P ON PB.post_id = P.parent # --line X-- needed so we can get date_posted, poster_id, etc
                    LEFT JOIN user_stars_post UL ON PB.post_id = UL.post_id AND UL.user_id = (%(uid)s)
                    WHERE UF.user_id = %(uid)s
                    AND reposted = 0 # otherwise we get duplicates i.e. both posts and reposts show cos of line X
                    ORDER BY date_added DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''

        elif mode == 'favourites-media':
            return \
                '''
                    SELECT pb.post_id, post_text, funny, not_funny, media_url, filename, poster_image,
                            true as favourited, media_type
                    FROM user_has_favs uf
                    JOIN post_body pb ON pb.post_id = uf.post_id
                    WHERE media_url IS NOT NULL AND user_id =%(uid)s
                    ORDER BY date_added DESC
                    LIMIT %(lim)s OFFSET %(off)s
                '''
