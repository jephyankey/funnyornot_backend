import logging as log
from Utilities import Utilities
from PostService import PostService
from DatabaseService import DatabaseService
from ValidationService import ValidationService
# import MySQLdb as mDb

__author__ = "henry"


class UserService:
    def __init__(self, db_service):
        self._dbs = db_service

    # todo ==============================
    # todo too much duplicate code: merge
    # todo ===============================

    # todo merge all possible into this function
    def perform_action(self, bound_values, mode):
        action = 'SELECT'
        if mode == 'follow':
            action = 'INSERT'

        query = self.write_query(mode)

        try:
            return self._dbs.run_prepared_query(query, bound_values, action)
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}

    # SPECIAL CASES (functions that do not fit not perform_action above)
    def get_avatar_urls(self, bound_values, user_string):

        """

        :param bound_values:
        :param user_string: a string with each value CAST TO INT of the same list
        :return: array of data
        """
        # TODO the only place we build a query with string concatenation
        # TODO keep making sure there are no holes here

        sql = self.write_query('avatar-urls') + user_string + ')'
        try:
            return self._dbs.run_prepared_query(sql, bound_values, "SELECT")
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def get_user_details(self, bound_values):
        prepared_query = self.write_query('user_details')
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, 'SELECT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def update_username(self, user_id, username):
        bv = {"user_id": user_id, "username": username}
        prepared_query = self.write_query('update-username')
        try:
            return self._dbs.run_prepared_query(prepared_query, bv, 'UPDATE')
        except Exception as e:
            if e.args and e.args[0] == 1062:   # duplicate username
                return {'fail': 'unavailable'}
            else:
                self.handle_errors(e)
                return {'fail': True}


    def update_device_id(self, user_id, device_id):
        bv = {"user_id": user_id, "device_id": device_id}
        prepared_query = self.write_query('update-device-id')
        try:
            return self._dbs.run_prepared_query(prepared_query, bv, 'UPDATE')
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def user_favourite(self,bound_values):
        prepared_query = self.write_query('user_favourite')
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, 'SELECT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def block_user(self, bound_values):
        prepared_query = self.write_query('user_block')
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, 'INSERT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def unblock_user(self, bound_values):
        prepared_query = self.write_query('unblock_user')
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, 'DELETE')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def follow_(self, bound_values, mode):
        if mode == 'follow':
            action = 'INSERT'
        else:
            action = 'DELETE'
        prepared_query = self.write_query(mode)
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, action)
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}

    # can be used to obtain LIST of ppl following a user/ number of poeple
    # a user is following
    # what is returned is based on the mode,
    # mode is following or followers
    def followers_(self, bound_values, mode):
        prepared_query = self.write_query(mode)
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, mode)
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    # can be used to obtain number of ppl following a user/ number of poeple
    # a user is following
    # what is returned is based on the mode,
    # mode is no-of-followers / no-of-following
    def no_of_followers(self, bound_values, mode):
        # mode is following or followers
        prepared_query = self.write_query(mode)
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values)
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def following_or_not(self, bound_values):
        prepared_query = self.write_query('following-or-not')
        try:
            return self._dbs.run_prepared_query(prepared_query, bound_values, 'SELECT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def set_as_funny(self, bound_values):
        sql1 = self.write_query('set-as-funny')
        try:
            # --- start a transaction----------
            self._dbs.start_transact()
            self._dbs.run_prepared_query(sql1, bound_values, 'INSERT')

            if bound_values["stars"] > 0:    # funny
                sql2 = self.write_query('update-funny')
            else:    # not funny
                sql2 = self.write_query('update-notfunny')
            new_bv = {"stars": abs(bound_values["stars"]), "post_id": bound_values["post_id"]}
            self._dbs.run_prepared_query(sql2, new_bv, 'UPDATE')

            # if 3 stars/smileys given re post
            if bound_values['stars'] == 3:
                obj_post = PostService(self._dbs)
                obj_post.repost(bound_values['user_id'], bound_values['post_id'])

            self._dbs.comm_it()
            # -------transaction ends here------
            return {'success': True}

        except Exception as e:
            self.handle_errors(e)
            self._dbs.roll_back()
            return {"fail": True}


    def find_friends(self, bv):
        try:
            return self._dbs.run_prepared_query(self.write_query('find-friends'), bv, 'SELECT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def get_fun_factor(self, user_id):
        sql = self.write_query('fun-factor')
        try:
            data = self._dbs.run_prepared_query(sql, {"uid": user_id}, 'SELECT')
            data[0]['fun_factor'] = Utilities.metric_prefix(data[0]['fun_factor'])
            return data[0]
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def update_avatar_url(self, avatar_url, filename, user_id):
        sql = self.write_query('update-avatar')
        bv = {"avatar_url": avatar_url, "user_id": user_id, "filename": filename}
        try:
            return self._dbs.run_prepared_query(sql, bv, 'UPDATE')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def get_avatar_filename(self, user_id):
        sql = self.write_query('get-avatar-filename')
        try:
            data = self._dbs.run_prepared_query(sql, {"user_id": user_id}, 'SELECT')
            return data[0]['avatar_filename']
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}



    def create_user(self, bv):
        try:
            vs = ValidationService()
            if vs.phone_no_in_use(bv['contact']):
                # returning user, check if the username is free, or that it BELONGED  to this same user
                if vs.uname_available(bv['username']) or vs.uname_belongs_phone(bv['username'], bv['contact']):
                    self._dbs.run_prepared_query(self.write_query('update-new-user'), bv, 'UPDATE')
                else:
                    return {"fail": 'unavailable'}

            else:
                if vs.uname_available(bv["username"]):
                    self._dbs.run_prepared_query(self.write_query('new-user'), bv, 'INSERT')
                else:
                    return {"fail": 'unavailable'}
            data = self._dbs.run_prepared_query(self.write_query('get-id-from-contact'),
                                                {"contact": bv['contact']}, 'SELECT')
            return data[0]  # data[0] = {"user_id": 345345453}

        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}

    # todo careful with security: if someone is able to get here without verification, and uses another person's phone
    # todo(ct'd) number, we will basically have two people with the same account. (one being a thief)


    # handle errors generated in this class
    def handle_errors(self, e):
        log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))
        return


    @staticmethod
    def write_query(mode):
        if mode == 'new-user':
            return \
                '''
                    INSERT INTO users (contact, username, country_code, device_id)
                    VALUES (%(contact)s, %(username)s, %(country_code)s, %(device_id)s)
                '''

        elif mode == 'update-new-user':
            return \
                '''
                    UPDATE users SET username = %(username)s, country_code = %(country_code)s, device_id = %(device_id)s
                    WHERE contact = %(contact)s
                '''

        elif mode == 'get-id-from-contact':
            return \
                '''
                    SELECT user_id FROM users WHERE contact =  %(contact)s
                '''

        elif mode == 'find-friends':
            return \
                '''
                    SELECT user_id, contact, device_id FROM users WHERE contact IN %(contacts)s
                    AND invisible = 0
                '''

        elif mode == 'user_details':
            return \
                 '''
                    SELECT * FROM users WHERE user_id = %(user_id)s
                 '''

        elif mode == 'update-username':
            return \
                '''
                    UPDATE users
                    SET  username = %(username)s WHERE user_id = %(user_id)s
                '''

        elif mode == 'update-device-id':
            return \
                '''
                    UPDATE users
                    SET  device_id = %(device_id)s WHERE user_id = %(user_id)s
                '''

        elif mode == 'update-avatar':
            return \
                '''
                    UPDATE users
                    SET avatar_url = %(avatar_url)s, avatar_filename = %(filename)s
                    WHERE user_id = %(user_id)s
                '''

        elif mode == 'user_block':
            return \
                    '''
                        INSERT INTO user_blocks_user(user_id, blocked_id)
                        VALUES (%(user_id)s, %(blocked_id)s)
                    '''

        elif mode == 'unblock_user':
            return \
                    '''
                        DELETE FROM user_blocks_user WHERE user_id = %(user_id)s AND blocked_id = %(blocked_id)s
                    '''

        elif mode == 'follow':
            return \
                    '''
                        INSERT INTO user_follows_user(follower, followed)
                         VALUES (%(follower)s, %(followed)s)
                    '''

        elif mode == 'unfollow':
            return \
                    '''
                        DELETE FROM user_follows_user WHERE  follower = (%(follower)s)
                        AND followed = (%(followed)s)
                    '''

        elif mode == 'following':
            return \
                    '''
                        SELECT * FROM user_follows_user WHERE  follower = (%(uid)s)
                    '''

        elif mode == 'followers':
            return \
                    '''
                        SELECT * FROM user_follows_user WHERE  followed = (%(uid)s)
                    '''

        elif mode == 'no-of-followers':
            return \
                    '''
                        SELECT count(follower) as total_followers FROM user_follows_user
                        WHERE  followed = (%(uid)s)
                    '''

        elif mode == 'no-of-following':
            return \
                    '''
                        SELECT count(followed) as total_following FROM user_follows_user
                        WHERE  follower = (%(uid)s)
                    '''

        elif mode == 'following-or-not':
            return \
                    '''
                        SELECT followed FROM user_follows_user WHERE follower = (%(follower)s)
                        AND followed = (%(followed)s)
                    '''

        elif mode == 'set-as-funny':
            return \
                    '''
                        INSERT INTO user_stars_post(user_id, post_id, stars)
                        VALUES (%(user_id)s, %(post_id)s, %(stars)s)
                        ON DUPLICATE KEY UPDATE stars = %(stars)s
                    '''

        elif mode == 'update-funny':
            return \
                    '''
                        UPDATE post_body SET funny = funny + %(stars)s WHERE post_id = %(post_id)s
                    '''

        elif mode == 'update-notfunny':
            return \
                    '''
                        UPDATE post_body SET not_funny = not_funny + %(stars)s WHERE post_id = %(post_id)s
                    '''

        elif mode == 'fun-factor':
            return \
                    '''
                        SELECT IFNULL(SUM(funny) - SUM(not_funny), 0) AS fun_factor
                        FROM post_body PB JOIN posts # join posts so we can get use poster_id
                        P ON PB.post_id = P.parent
                        WHERE reposted = 0
                        AND poster_id = %(uid)s
                    '''

        elif mode == 'get-avatar-filename':
            return \
                    '''
                        SELECT avatar_filename FROM users WHERE user_id = %(user_id)s
                    '''

        # VERY SPECIAL CASE
        # we are unable to pass the second list as bound parameter cos if it is quoted, query fails,
        # so we return part here and join the rest in the function above
        # hence, security is left to the calling fx
        elif mode == 'avatar-urls':
            return \
                    '''
                        SELECT user_id, avatar_url FROM users WHERE user_id IN %(user_list)s
                        ORDER BY FIELD(user_id,
                    '''

