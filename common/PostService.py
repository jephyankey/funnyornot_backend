import logging as log
from time import time


class PostService:
    def __init__(self, db_service):
        self._dbs = db_service


    def create_post(self, user_id, text, media_type, media_url, filename, poster_image):
        sql1 = self.write_query('post-body')
        bv = {"new_text":text, "media_type":media_type, "media_url":media_url,
                            "filename":filename, "poster_image": poster_image}
        sql2 = self.write_query('post')
        try:
            #--- start a transaction----------
            self._dbs.start_transact()
            self._dbs.run_prepared_query(sql1, bv, 'INSERT')
            # get the id of the post we just inserted
            parent = self._dbs.get_last_id()
            # create set of bound values and use to post
            self._dbs.run_prepared_query(sql2, {"date_posted": int(time()), "poster_id": user_id, "parent": parent})
            self._dbs.comm_it()
            #-------transaction ends here------
            return {'success': True, 'post_id': parent}
        except Exception as e:
            self.handle_errors(e)
            self._dbs.roll_back()
            return {'fail': True}


    def block_post(self, user_id, post_id):
        sql = self.write_query('block-post')
        try:
            return self._dbs.run_prepared_query(sql, {"uid": user_id, "post_id": post_id}, 'INSERT')
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def repost(self, user_id, parent):
        sql = self.write_query('repost')
        bv = {"date_posted": int(time()), "poster_id": user_id, "parent": parent}
        try:
            return self._dbs.run_prepared_query(sql, bv, 'INSERT')
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def add_favourite(self, user_id, post_id):
        sql = self.write_query('add-favourite')
        bv = {"uid": user_id, "post_id": post_id, "date_added": int(time())}
        try:
            return self._dbs.run_prepared_query(sql, bv, 'INSERT')
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def remove_favourite(self, user_id, post_id):
        sql = self.write_query('remove-favourite')
        try:
            return self._dbs.run_prepared_query(sql, {"uid": user_id, "post_id": post_id}, 'DELETE')
        except Exception as e:
            self.handle_errors(e)
            return {'fail': True}


    def report_abuse(self, post_id, reporter_id):
        sql = self.write_query('report-abuse')
        bv = {"post_id": post_id, "reporter_id": reporter_id}
        try:
            return self._dbs.run_prepared_query(sql, bv, 'INSERT')
        except Exception as e:
            if e.args and e.args[0] == 1062:   #post reported already
                return {'fail': 'already done'}
            self.handle_errors(e)
            return {'fail': True}


    def unblock_all_posts(self, user_id):
        sql = self.write_query('unblock-all')
        try:
            return self._dbs.run_prepared_query(sql, {"uid": user_id}, 'DELETE')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    # gracefully handle errors generated in this class
    def handle_errors(self, e):
        log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))
        return


    @staticmethod
    def write_query(mode):
        if mode == 'post-body':
            return \
                '''
                    INSERT INTO post_body (post_text, media_type, media_url, filename, poster_image)
                    VALUES (%(new_text)s, %(media_type)s, %(media_url)s, %(filename)s, %(poster_image)s)
                '''
        elif mode == 'post':
            return \
                '''
                    INSERT INTO posts (date_posted, poster_id, parent)
                    VALUES (%(date_posted)s, %(poster_id)s, %(parent)s)
                '''
        elif mode == 'repost':
            return \
                '''
                    INSERT INTO posts (date_posted, poster_id, parent, reposted)
                    VALUES (%(date_posted)s, %(poster_id)s, %(parent)s, 1)
                '''

        elif mode == 'block-post':
            return \
                '''
                    INSERT INTO user_blocks_post VALUES (%(uid)s, %(post_id)s)
                '''
        elif mode == 'add-favourite':
            return \
                '''
                    INSERT INTO user_has_favs VALUES (%(uid)s, %(post_id)s, %(date_added)s)
                '''

        elif mode == 'remove-favourite':
            return \
                '''
                    DELETE FROM user_has_favs WHERE user_id = %(uid)s
                    AND post_id = %(post_id)s
                '''

        elif mode == 'report-abuse':
            return \
                '''
                    INSERT INTO abuse_reports VALUES (%(post_id)s, %(reporter_id)s)
                '''

        elif mode == 'unblock-all':
            return \
                '''
                    DELETE FROM user_blocks_post WHERE user_id = %(uid)s
                '''