import logging as log


class SettingsService:

    def __init__(self, db_service):
        self._dbs = db_service


    def set_visibility(self, user_id, value):
        sql = self.write_query('visibility')
        try:
            return self._dbs.run_prepared_query(sql, {"val": value, "uid": user_id}, 'UPDATE')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def who_sees_dp(self, user_id, value):
        sql = self.write_query('dpscope')
        try:
            return self._dbs.run_prepared_query(sql, {"val": value, "uid": user_id}, 'UPDATE')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def get_help_topics(self):
        sql = self.write_query('help')
        try:
            return self._dbs.run_prepared_query(sql, {})
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def get_help_details(self, topic_id):
        sql = self.write_query('help-content')
        try:
            return self._dbs.run_prepared_query(sql, {"id": topic_id}, 'SELECT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def help_add(self, topic, content):
        sql = self.write_query('help-add')
        try:
            return self._dbs.run_prepared_query(sql, {"topic": topic, "content": content}, 'INSERT')
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    #  handle errors generated in this class
    def handle_errors(self, e):
        log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))
        return


    @staticmethod
    def write_query(mode):
        if mode == 'visibility':
            return \
                '''
                    UPDATE users SET invisible = (%(val)s) WHERE user_id = (%(uid)s)
                '''

        elif mode == 'dpscope':
            return \
                '''
                    UPDATE users SET who_sees_dp = (%(val)s) WHERE user_id = (%(uid)s)
                '''

        elif mode == 'help':
            return \
                '''
                    SELECT id, topic FROM help
                '''

        elif mode == 'help-content':
            return \
                '''
                    SELECT content FROM help WHERE id = (%(id)s)
                '''

        elif mode == 'help-add':
            return \
                '''
                    INSERT INTO help (topic, content) VALUES (%(topic)s, %(content)s)
                '''