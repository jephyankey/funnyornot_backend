__author__ = "MArK"


# imports
import logging as log
from DatabaseService import DatabaseService



class CronService:
    def __init__(self, db_service):
        """
        Constructor.

        :param db_service(DatabaseService): the database service object to execute queries with
        """
        self.__dbs = db_service


    def clear_temporary_data(self, dtime):
        """
        Clears the temporary data in the temp_users table.

        :param dtime(time): the time value to use to filter out data that have expired

        :return: data | {'fail': true}
        """

        sql = self.write_query('clear-temporary-data')
        try:
            return  self.__dbs.run_prepared_query(sql, {"dtime": dtime}, "DELETE")
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}




    def handle_errors(self, e):
        """
        Handles errors raised in this class by logging them.

        :param e(Error): the error raised

        :return:
        """
        log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))
        return



    @staticmethod
    def write_query(mode):
        if mode == 'clear-temporary-data':
            return \
                '''
                    DELETE FROM temp_users WHERE dtime <= %(dtime)s
                '''