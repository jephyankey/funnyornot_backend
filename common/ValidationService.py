import logging as log
from random import random
from math import ceil
from time import time
from DatabaseService import DatabaseService
from SmsService import SmsService as Sms


class ValidationService:
    def __init__(self):
        self._dbs = DatabaseService()


    def validate_new_no(self, phone_number):
        verification_code = str(int(ceil(random() * 10000)))
        if len(verification_code) == 3:
            verification_code = '0'+verification_code
        # verification_code = '0000'
        date = int(time())
        sql = self.write_query('create-temp-user')
        bound_values = {"phone": phone_number, "code": verification_code, "dtime": date}
        try:
            # first create the user in temporary table
            self._dbs.run_prepared_query(sql, bound_values, 'INSERT')
            # now send an SMS to user's phone_number
            msg = "Hi, your FunnyOrNot PIN is {}, Enjoy!".format(verification_code)
            Sms.send_sms(msg, phone_number)
            return {"success": True}

        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def confirm_verification_code(self, code, phone_number):
        bound_values = {"phone": phone_number, "code": code}
        sql = self.write_query('confirm-code')
        sql2 = self.write_query('update-confirmation')
        try:
            data = DatabaseService().run_prepared_query(sql, bound_values, 'SELECT')
            if len(data) > 0:
                DatabaseService().run_prepared_query(sql2, bound_values, 'UPDATE')
            return data
        except Exception as e:
            self.handle_errors(e)
            return {"fail": True}


    def uname_available(self, username):
        # now check to see if the username is available
        sql = self.write_query('check-username')
        bv = {"username": username}
        name = self._dbs.run_prepared_query(sql, bv, 'SELECT')
        if len(name) == 0: # username is available
            return True
        else:
            return False

    def phone_no_in_use(self, phone_no):
        # check to see if a given phone number has used the app before
        phone = self._dbs.run_prepared_query(self.write_query('check-phone_no'), {"contact": phone_no}, 'SELECT')
        if len(phone) == 0:
            return False
        else:
            return True

    def uname_belongs_phone(self, username, phone_no):
        result = self._dbs.run_prepared_query(self.write_query('uname-against-phone'),
                                              {"username": username, "contact": phone_no}, 'SELECT')
        if len(result) == 0:
            return False
        else:
            return True

    @staticmethod
    def write_query(mode="random"):
        if mode == 'create-temp-user':
            return \
                '''
                    INSERT INTO temp_users (phone_number, verification_code, dtime)
                    VALUES (%(phone)s, %(code)s, %(dtime)s)
                    ON DUPLICATE KEY UPDATE verification_code = %(code)s, dtime = %(dtime)s
                '''

        elif mode == 'confirm-code':
            return \
                '''
                    SELECT * FROM temp_users WHERE phone_number = %(phone)s
                    AND verification_code = %(code)s
                '''

        elif mode == 'update-confirmation':
            return \
                '''
                    UPDATE temp_users SET verified = 1 WHERE phone_number = %(phone)s
                    AND verification_code = %(code)s
                '''

        elif mode == 'check-username':
            return \
                '''
                    SELECT username FROM users WHERE username = %(username)s
                '''

        elif mode == 'uname-against-phone':
            return \
                '''
                    SELECT user_id FROM users WHERE username = %(username)s AND contact = %(contact)s
                '''

        elif mode == 'check-phone_no':
            return \
                '''
                    SELECT contact FROM users WHERE contact = %(contact)s
                '''

    def handle_errors(self, e):
        log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))
