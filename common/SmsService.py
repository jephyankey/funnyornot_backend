k__author__ = "MArK"


from plivo import RestAPI
import logging as log

AUTH_ID = "MAYZAYMJA3ZMRJMZU3OT"
AUTH_TOKEN = "YmJhODVmYzFiZDA4Mzg1MjFmOTVmNzgyMWViMDRl"
SOURCE = "16267411492"


class SmsService():
    @staticmethod
    def send_sms(message, destination):
        """
        Send an SMS message to a phone number.

        :param message(string) the message to be sent
        :param destination(string) the recipient phone number

        :returns True | False
        """

        try:
            client = RestAPI(AUTH_ID, AUTH_TOKEN)

            # params for message
            params = {
                "src":SOURCE,
                "dst": str(destination),
                "text": str(message)
            }

            response = client.send_message(params)

        except:
            return False

        return True