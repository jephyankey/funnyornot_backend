from datetime import datetime


class Utilities:

    @staticmethod
    def timesince(timestamp, default="just now"):

        diff = datetime.utcnow() - datetime.fromtimestamp(timestamp)

        periods = (
            (diff.days / 365, "year", "years"),
            (diff.days / 30, "month", "months"),
            (diff.days / 7, "week", "weeks"),
            (diff.days, "day", "days"),
            (diff.seconds / 3600, "hour", "hours"),
            (diff.seconds / 60, "minute", "minutes"),
            (diff.seconds, "second", "seconds"),
        )

        for period, singular, plural in periods:

            if period:
                return "%d %s" % (period, singular if period == 1 else plural)

        return default

    @staticmethod
    def metric_prefix(num):
        for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
            if num < 1000:
                return '{:.1f}{}'.format(num, unit)
            num /= 1000
        return str(num)
