# -*- coding: UTF-8 -*-
import MySQLdb as mDb
import logging as log
import os
import global_config as G

__author__ = "Jeph"


class DatabaseService:
    def __init__(self):
        try:
            # When running on Google App Engine
            if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
                self._conn = mDb.connect(unix_socket=G.AE_HOST, user=G.AE_USER, passwd=G.AE_PASSWORD, db=G.AE_DATABASE)
            else:  # running locally
                self._conn = mDb.connect(G.HOST, G.USER, G.PASSWORD, G.DATABASE)
            self._cursor = self._conn.cursor(mDb.cursors.DictCursor)
            self.multiple_queries = False
        except mDb.Error, e:
            log.error('Error in class %s :--- %s' % (self.__class__.__name__, e))

    def run_prepared_query(self, sql, bound_values, mode='SELECT', dim='single'):
        """
        light interface for CRUD operations
        :param sql: prepared sql query
        :param bound_values: array (tuple, list or dict) of parameters
        :param mode: SELECT, UPDATE, DELETE, INSERT to specify the type of database operation
        :param dim: 'single' or 'multi' to determine whether we have a single array of parameters or a plural set
        :return:  if SELECT query - SQL result set as multidimensional dict
                    else - {'success': True} | {'fail': True}
        """
        # multidimensional bound_values (possible for for C, U and D operations), in which case query is executed
        #  ones, but with multiple values
        if dim == 'multi':
            self._cursor.executemany(sql, bound_values) # todo
        # single dimensional bound values
        else:
            self._cursor.execute(sql, bound_values)

        # commit immediately, and return results, if its just a single query
        if not self.multiple_queries:
            self._conn.commit()
            # self._conn.close()
            # for R operations, return the entire result set
            if mode == 'SELECT':
                return self._cursor.fetchall()
            # but for C, U, D operations an array indicating operation status
            else:
                return {'success': True}

    def start_transact(self):
        self.multiple_queries = True

    def roll_back(self):
        if self._conn:
            self._conn.rollback()
            # self._conn.close()

    def comm_it(self):
        if self._conn:
            self._conn.commit()
            # self._conn.close()

    def get_last_id(self):
        return self._conn.insert_id()    # you can also use lastrowid() on the cursor instead